<?php
require_once 'includes/connect.php';

$sql = "SELECT * 
FROM `objects`";


if ($_GET['category']) {
	$params = explode('-', $_GET['category']);
	$i = 0;
	foreach ($params as $v) {
		if ($i === 0) {
			$sql .= " WHERE `category` = '". $v ."'";
		} else {
			$sql .= " OR `category` = '". $v ."'";
		}
		$i++;
	}
}

$stmt = $pdo->prepare($sql);
$stmt->execute();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<form action="createLink.php" method="post">
		<div>
			<label for="deco">Déco</label>
			<input type="checkbox" name="deco" id="deco">
		</div>
		<div>
			<label for="objet">Objet</label>
			<input type="checkbox" name="objet" id="objet">
		</div>
		<input type="submit">
	</form>

	<table>
		<tr>
			<th>#</th>
			<th>name</th>
			<th>category</th>
		</tr>
		<?php while ($object = $stmt->fetch(PDO::FETCH_ASSOC)):?>
			<tr>
				<td>
					<?=$object['id'] ?>
				</td>
				<td>
					<?=$object['name']?>
				</td>
				<td>
					<?=$object['category']?>
				</td>
			</tr>
		<?php endwhile ?>
	</table>
</body>
</html>
